/**
 * @file KolmogorovEquation.cpp
 * @authors Hedi Chaibi, Sami Khan
 * @date 25/02/2020
 * @brief File containing Methods for the Kolmogorov class and derived classes.
 */

#include "KolmogorovEquation.h"

using namespace std;
using namespace arma;

Kolmogorov::Kolmogorov(double x0, double xM, double t0, double tN, double dx, double dt, string f, string g,
                       string h, string tc, string bc_top, string bc_bottom) {
        this->f = f;
        this->g = g;
        this->h = h;
        this->tc = tc;
        this->bc_top = bc_top;
        this->bc_bottom = bc_bottom;
        this->xM = xM;
        this->x0 = x0;
        this->t0 = t0;
        this->dx = dx;
        this->dt = dt;

        /// Cauchy Criterion, warns the user but does not end the program.
        if (dt / pow(dx, 2) >= 1) {
                cerr << "Warning: The CFL (dt/dx^2 < 1) is not satisfied, convergence might not be achieved."
                     << endl;
        }

        /// The maximum of each value must be greater than the minimum.
        if (xM < x0 || tN < t0) {
                throw logic_error("Ensure x/t min and x/t max are in the correct order.");
        }

        /// The steps cannot be negative or 0 (program would never end).
        if (dx <= 0 || dt <= 0) {
                throw logic_error("dx and dt must be strictly positive.");
        }

        /// If the steps are too large there will be nothing to calculate.
        if (2 * dx > (xM - x0) || 2 * dt > (tN - t0)) {
                throw logic_error("dx and dt too large compared to respective ranges.");
        }

        noX = (xM - x0) / dx + 1; /// Number of Xs (inclusive hence +1)
        noT = (tN - t0) / dt + 1; /// Number of ts (inclusive hence +1)

        const string expression_string[6] = {f, g, h, tc, bc_top,
                                             bc_bottom}; /// Adding all strings in preparation for conversion into functions.

        symbol_table_t symbol_table;
        symbol_table.add_variable("x", x); /// Telling exprt that x and t are considered variables.
        symbol_table.add_variable("t", t);
        symbol_table.add_constants(); /// Adding constants such as Pi, e, ... for mathematical calculations.

        expression_t expression;
        expression.register_symbol_table(symbol_table);

        parser_t parser;
        for (size_t i = 0; i < 6; ++i) {
                /// Here we try to compile the function created from the string.
                if (parser.compile(expression_string[i], expression)) {
                        expression_list[i] = expression;
                } else {
                        throw invalid_argument("Invalid Input (check \"" + expression_string[i] + "\")");
                }
        }

        C = mat(noX, noT, fill::zeros); /// Matrix of doubles of size noX x noT filled with zeros.
        F = mat(noX, noT, fill::zeros);
        G = mat(noX, noT, fill::zeros);
        H = mat(noX, noT, fill::zeros);
        tri_diag_left = mat(noX - 2, noX - 2, fill::zeros);
        tri_diag_right = mat(noX - 2, noX - 2, fill::zeros);

        bc_left = vec(noX - 2, fill::zeros); /// Vector of doubles of size noX-2 filled with zeros.
        bc_right = vec(noX - 2, fill::zeros);


        /// Filling in the F, G and H matrices using the previously created functions.
        for (int i = 0; i < noX; i++) {
                for (int j = 0; j < noT; j++) {
                        F(i, j) = 0.5 * (dt / pow(dx, 2)) * f_func(x0+i*dx,t0+j*dt) +
                                  0.25 * (dt / dx) * g_func(x0+i*dx,t0+j*dt);
                        G(i, j) =
                                (dt / pow(dx, 2)) * f_func(x0+i*dx,t0+j*dt) - 0.5 * dt * h_func(x0+i*dx,t0+j*dt);
                        H(i, j) = 0.5 * (dt / pow(dx, 2)) * f_func(x0+i*dx,t0+j*dt) -
                                  0.25 * (dt / dx) * g_func(x0+i*dx,t0+j*dt);
                }
        }
}

mat Forward::finite_difference(string method) {

        /// Filling in the initial condition in the first column of the output matrix.
        for (int i = 0; i < noX; i++) {
                C(i, 0) = terminal_condition(x0 + i * dx);
        }

        /// Filling in the top and bottom boundary conditions in the first and last row of the output matrix.
        for (int i = 0; i < noT; i++) {
                C(0, i) = top_bc(x0, t0 + i * dt);
                C(noX - 1, i) = bottom_bc(xM, t0 + i * dt);
        }

        for (int tau = 0; tau < noT; tau++) {

                bc_right(0) = H(1, tau) * C(0, tau);
                bc_left(noX - 3) = -F(noX - 2, tau) * C(noX - 1, tau);

                for (int i = 0; i < noX - 2; i++) {
                        for (int j = 0; j < noX - 2; j++) {
                                if (i == j) {
                                        tri_diag_right(i, j) = 1 - G(i + 1, tau);
                                }
                                if (i == j + 1) {
                                        tri_diag_right(i, j) = H(i + 1, tau);
                                }
                                if (i == j - 1) {
                                        tri_diag_right(i, j) = F(i + 1, tau);
                                }
                        }
                }

                vec mat_mul_right;

                if (tau > 0) {

                        vec previousF(C.submat(1, tau - 1, noX - 2, tau - 1)); /// Previous column, excluding first and last rows.
                        vec mat_mul_left = tri_diag_left * previousF + bc_left - bc_right;

                        /// Using arma::solve which solves using the Gauss-Jordan method.
                        try {
                                mat_mul_right = solve(tri_diag_right, mat_mul_left).eval();
                        }
                        catch (runtime_error &e) {
                                throw e;
                        }

                        /// Filling in the current column in the matrix.
                        for (int i = 1; i < noX - 1; i++) {
                                C(i, tau) = mat_mul_right(i - 1, 0);
                        }
                }

                bc_left = -bc_right;

                tri_diag_left = -tri_diag_right + 2 * eye(noX-2, noX-2);
        }

        return C;
}

mat Backward::finite_difference(string method) {

        /// Filling in the terminal condition in the last column of the output matrix.
        for (int i = 0; i < noX; i++) {
                C(i, noT - 1) = terminal_condition(x0 + i * dx);
        }

        for (int i = 0; i < noT; i++) {
                C(0, i) = top_bc(x0, t0 + i * dt);
                C(noX - 1, i) = bottom_bc(xM, t0 + i * dt);
        }

        for (int tau = noT - 1; tau >= 0; tau--) {

                bc_left(0) = -H(1, tau) * C(0,tau);
                bc_left(noX-3) = -F(noX-2, tau) * C(noX - 1, tau);

                for (int i = 0; i < noX-2; i++) {
                        for (int j = 0; j < noX-2; j++) {
                                if (i == j) {
                                        tri_diag_left(i, j) = 1 + G(i+1, tau);
                                }
                                if (i == j + 1) {
                                        tri_diag_left(i, j) = -H(i+1, tau);
                                }
                                if (i == j - 1) {
                                        tri_diag_left(i, j) = -F(i+1, tau);
                                }
                        }
                }

                vec mat_mul_left;

                if(tau < noT-1) {

                        vec previousF(C.submat(1, tau + 1, noX - 2, tau + 1));
                        vec mat_mul_right = tri_diag_right * previousF + bc_right - bc_left;

                        try {

                          //This would use Thomas algorithm to solve the system of equations.

                          if (method == "Thomas") {
                            mat_mul_left = solve(tri_diag_left, mat_mul_right).eval();
                          }

                          //This method uses matrix inversion.

                          else if (method == "Inversion"){

                            mat inv_tri_diag_left = inv(tri_diag_left);
                            mat_mul_left = inv_tri_diag_left*mat_mul_right;

                          }

                          else{

                            cerr << "Invalid method input: must be 'Thomas' (default) or 'Inversion'.\n";
                            exit(INVALID_INPUT);

                          }

                        }
                        catch (runtime_error &e) {
                                throw e;
                        }

                        for (int i = 1; i < noX - 1; i++) {
                                C(i, tau) = mat_mul_left(i - 1, 0);
                        }
                }

                bc_right = -bc_left;

                tri_diag_right = -tri_diag_left + 2 * eye(noX-2, noX-2);
        }


        return C;
}

double Kolmogorov::f_func(double x, double t) {
        this->x = x; /// Setting the current value of x to the given value as this is the value exprtk library will use.
        this->t = t;
        auto output_value = expression_list[0].value();

        /// Any mathematical error will result in a nan value.
        if (isnan(output_value)) {
                throw invalid_argument("Generated nan value (Check f function)");
        }

        return output_value;
}

double Kolmogorov::g_func(double x, double t) {
        this->x = x;
        this->t = t;
        auto output_value = expression_list[1].value();

        if (isnan(output_value)) {
                throw invalid_argument("Generated nan value (Check g function)");
        }

        return output_value;
}

double Kolmogorov::h_func(double x, double t) {
        this->x = x;
        this->t = t;
        auto output_value = expression_list[2].value();

        if (isnan(output_value)) {
                throw invalid_argument("Generated nan value (Check h function)");
        }

        return output_value;
}

double Kolmogorov::terminal_condition(double x) {
        this->x = x;
        this->t = 0;
        return expression_list[3].value();
}

double Kolmogorov::top_bc(double x, double t) {
        this->x = 0;
        this->t = t;
        return expression_list[4].value();
}

double Kolmogorov::bottom_bc(double x, double t) {
        this->x = 0;
        this->t = t;
        return expression_list[5].value();
}
