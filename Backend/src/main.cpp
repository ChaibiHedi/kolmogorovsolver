/**
 * @file main.cpp
 * @authors Hedi Chaibi, Sami Khan
 * @date 25/02/2020
 * @brief File containing the main method for the Kolmogorov program.
 */

#include "KolmogorovEquation.h"

using namespace std;
using namespace arma;

int main(int argc, char **argv) {

        /// Ensure there is enough information for the program to work.
        if (argc != 14 && argc != 15) {
            cerr << "Not enough arguments." << endl;
            return ILLEGAL_ARGUMENT_NUMBER;
        }

        /// Ensure the numeric inputs (displayed below as doubles) do not contain non-numeric characters.
        for (int i = 2; i < 8; i++) {
            for (unsigned j = 0; j < strlen(argv[i]); j++) {
                if (!isdigit(argv[i][j]) && argv[i][j] != '-' && argv[i][j] != '.') {
                    cerr << "Invalid characters in \"" << argv[i] << "\"" << endl;
                    return INVALID_INPUT;
                }
            }
        }

        bool forward = atoi(argv[1]);
        double x0 = atof(argv[2]);
        double xM = atof(argv[3]);
        double t0 = atof(argv[4]);
        double tN = atof(argv[5]);
        double dx = atof(argv[6]);
        double dt = atof(argv[7]);
        string f = argv[8];
        string g = argv[9];
        string h = argv[10];
        string bc = argv[11];
        string bc_top = argv[12];
        string bc_bottom = argv[13];
        bool inversion_method = false;

        if (argc == 15) {
          inversion_method = atoi(argv[14]);
        }

        mat output;

        if (forward) {
            try {
                auto forwardEquation = Forward(x0, xM, t0, tN, dx, dt, f, g, h, bc, bc_top, bc_bottom);

                if(inversion_method){
                  output = forwardEquation.finite_difference("Inversion");
                }
                else{
                  output = forwardEquation.finite_difference();
                }

            }
            catch (invalid_argument &e) {
                cerr << e.what() << endl;
                return INVALID_INPUT;
            }
            catch (runtime_error &e) {
                cerr << e.what() << endl;
                return NO_SOLUTION;
            }
            catch (logic_error &e) {
                cerr << e.what() << endl;
                return LOGIC_ERROR;
            }
        } else {
            try {
                auto backwardEquation = Backward(x0, xM, t0, tN, dx, dt, f, g, h, bc, bc_top, bc_bottom);

                if(inversion_method){
                  output = backwardEquation.finite_difference("Inversion");
                }
                else{
                  output = backwardEquation.finite_difference();
                }

            }
            catch (invalid_argument &e) {
                cerr << e.what() << endl;
                return INVALID_INPUT;
            }
            catch (runtime_error &e) {
                cerr << e.what() << endl;
                return NO_SOLUTION;
            }
            catch (logic_error &e) {
                cerr << e.what() << endl;
                return LOGIC_ERROR;
            }
        }

        /// Save the file as a csv for the user.
        output.save("output.csv", csv_ascii);

    return 0;
}
