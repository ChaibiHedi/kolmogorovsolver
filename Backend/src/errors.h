/**
 * @file errors.h
 * @authors Hedi Chaibi, Sami Khan
 * @date 25/02/2020
 * @brief File containing the errors produced by the program.
 */

#ifndef errors_h
#define errors_h

#define NO_SOLUTION 1
#define INVALID_INPUT 2
#define ILLEGAL_ARGUMENT_NUMBER 3
#define LOGIC_ERROR 4

#endif /* errors_h */
