/**
 * @file KolmogorovEquation.h
 * @authors Hedi Chaibi, Sami Khan
 * @date 25/02/2020
 * @brief File containing classes for forward and backward Kolmogorov equations.
 *
 * The parent class is Kolmogorov and there are two derived classes namely Forward and Backward. Both the derived
 * classes have a finite_difference method which carries out the main maths involved in producing the output matrix.
 */

#ifndef KOLMOGOROVEQUATION_H
#define KOLMOGOROVEQUATION_H

#include <iostream>
#include <cassert>
#include <string>
#include <cmath>
#include <armadillo>
#include <stdexcept>
#include "../lib/exprtk.hpp"
#include "errors.h"

/**
 * Kolmogorov Class
 * @brief Parent class mostly virtual aside from the constructor which is used by the derived classes.
 */
class Kolmogorov {
protected:
    /// Used for exprtk functions later in the code.
    typedef exprtk::symbol_table<double> symbol_table_t;
    typedef exprtk::expression<double> expression_t;
    typedef exprtk::parser<double> parser_t;

    arma::mat F, G, H, tri_diag_left, tri_diag_right, C;
    arma::vec bc_left, bc_right;

    /// List of string functions which will be passed into the function.
    expression_t expression_list[6];

    double dx, dt, x0, xM, t0, x, t;
    int noX, noT;

    std::string f, g, h, tc, bc_top, bc_bottom;

public:
    /**
     * Constructor - Prepares the equation for calculation.
     *
     * @param x0 Minimum value of spatial coordinate to be displayed.
     * @param xM Maximum value of spatial coordinate to be displayed.
     * @param t0 Minimum value of time to be displayed.
     * @param tN Maximum value of time to be displayed.
     * @param dx Size of spatial step.
     * @param dt Size of time step.
     * @param f f(x,t) function in the Kolmogorov equation.
     * @param g g(x,t) function in the Kolmogorov equation.
     * @param h h(x,t) function in the Kolmogorov equation.
     * @param tc The initial or terminal condition for the equation system (depends only on x).
     * @param bc_top The values of C at the maximum spatial coordinate (depends only on t).
     * @param bc_bottom The values of C at the minimum spatial coordinate (depends only on t).
     */
    Kolmogorov(double x0, double xM, double t0, double tN, double dx, double dt, std::string f, std::string g,
               std::string h, std::string tc, std::string bc_top, std::string bc_bottom);

    /**
     * Finite Difference method for calculating the solution of the provided system.
     *
     * @return A matrix representing the solution C of the system.
     */
    virtual arma::mat finite_difference(std::string method) = 0;

    /**
     * Function created from the tc string given by the user.
     *
     * @param x The position at which the terminal condition is to be evaluated.
     * @return The result of the evaluation of the function at the given position.
     */
    double terminal_condition(double x);

    /**
     * Function created from the f string given by the user.
     *
     * @param x The position at which this function is to be evaluated.
     * @param t The time at which this function is to be evaluated.
     * @return The result of the evaluation of the function at the given position and time.
     */
    double f_func(double x, double t);

    /**
     * Function created from the g string given by the user.
     *
     * @param x The position at which this function is to be evaluated.
     * @param t The time at which this function is to be evaluated.
     * @return The result of the evaluation of the function at the given position and time.
     */
    double g_func(double x, double t);

    /**
     * Function created from the h string given by the user.
     *
     * @param x The position at which this function is to be evaluated.
     * @param t The time at which this function is to be evaluated.
     * @return The result of the evaluation of the function at the given position and time.
     */
    double h_func(double x, double t);

    /**
     * Function created from the bc_top string given by the user.
     *
     * @param t The time at which this function is to be evaluated.
     * @return The result of the evaluation of the function at the given position and time.
     */
    double top_bc(double x, double t);

    /**
     * Function created from the bc_bottom string given by the user.
     *
     * @param t The time at which this function is to be evaluated.
     * @return The result of the evaluation of the function at the given position and time.
     */
    double bottom_bc(double x, double t);
};

/**
 * Forward Class
 * @brief Here the initial condition is given.
 */
class Forward : public Kolmogorov {
public:
    /**
     * Follows same layout as constructor defined in parent class.
     */
    Forward(double x0, double xM, double t0, double tN, double dx, double dt, std::string fF, std::string gF,
            std::string hF, std::string bcF, std::string bc_top, std::string bc_bottom) : Kolmogorov(x0, xM, t0, tN, dx,
                                                                                                     dt, fF, gF, hF,
                                                                                                     bcF, bc_top,
                                                                                                     bc_bottom) {};

    /**
     * Follows same layout as virtual function defined in parent class.
     */
    arma::mat finite_difference(std::string method = "Thomas") override;
};

/**
 * Forward Class
 * @brief Here the terminal condition is given.
 */
class Backward : public Kolmogorov {
public:
    /**
     * Follows same layout as constructor defined in parent class.
     */
    Backward(double x0, double xM, double t0, double tN, double dx, double dt, std::string fF, std::string gF,
             std::string hF, std::string bcF, std::string bc_top, std::string bc_bottom) : Kolmogorov(x0, xM, t0, tN,
                                                                                                      dx, dt, fF, gF,
                                                                                                      hF, bcF, bc_top,
                                                                                                      bc_bottom) {};

    /**
     * Follows same layout as virtual function defined in parent class.
     */
    arma::mat finite_difference(std::string method = "Thomas") override;
};


#endif //KOLMOGOROVEQUATION_H
