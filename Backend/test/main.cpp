#include "testing.h"
#include "utils.h"
using namespace std;

int main(int argc, char** argv){

  if(argc != 3 && argc != 4){
    cerr << "Must provide path to backend executable, then input string in format: <size1>,<size2>,<size3>";
    return 1;
  }

  char* path_to_executable = argv[1];
  char* sizes = argv[2];

  bool inversion = false;

  if(argc == 4){
    inversion = atoi(argv[3]);
  }

  test(sizes, path_to_executable, inversion);

  return 0;
}
