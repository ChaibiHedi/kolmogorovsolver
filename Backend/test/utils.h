#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <cstring>
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>
using namespace std;

double BSCallPrice(double x, double strike, double vol, double tt, double r);
double norm_cdf(const double x);
int count_char(const char* input, char character);
vector<string> split_string(const char* input, char delimiter);
vector<int> convert_vec_string_to_vec_double(vector<string> input);
void print_vector(vector<int> my_vector);
void export_to_csv(string filename, vector<int> myvector, vector<double> times);

#endif
