#include "testing.h"

void test(const char* sizes, const char* path, bool inversion){

        vector<string> string_sizes = split_string(sizes, ',');
        vector<int> dimensions = convert_vec_string_to_vec_double(string_sizes);
        vector<double> times;
        vector<double> errors_vector;

        double cfl_ratio = 0.75;

        string forward = "0";
        double x0 = 0;
        double xM = 200;
        double t0 = 0;
        double tM = 2;
        string f = "0.5 * 0.01 * x^2";
        string g = "0.0025*x";
        string h = "-0.0025";
        string bc = "(x>100)?x-100:0";
        string bc_bottom = "0";
        string bc_top = "100*exp(-0.0025*(2-t))";


        for(int i = 0; i < dimensions.size(); i++) {

                int dimension = dimensions[i];

                cout << "Evaluating time for N = " << dimension << endl;

                int NoX = std::ceil(std::cbrt((cfl_ratio * dimension * pow(xM - x0, 2))/(tM-t0)));
                int NoT = std::ceil(dimension/NoX);

                double dx = (xM - x0)/NoX;
                double dt = (tM - t0)/NoT;

                string dx_string = to_string(dx);
                string dt_string = to_string(dt);
                string x0_string = to_string(x0);
                string xM_string = to_string(xM);
                string t0_string = to_string(t0);
                string tM_string = to_string(tM);

                // assert(dt/pow(dx,2) < 1);

                string path_to_backend(path);

                string command = path_to_backend +
                                 " " + forward
                                 + " " + x0_string
                                 + " " + xM_string
                                 + " " + t0_string
                                 + " " + tM_string
                                 + " " + dx_string
                                 + " " + dt_string
                                 + " " + '"' + f + '"'
                                 + " " + '"' + g + '"'
                                 + " " + '"' + h + '"'
                                 + " " + '"' + bc + '"'
                                 + " " + '"' + bc_bottom + '"'
                                 + " " + '"' + bc_top + '"';

                string method_parameter = " 1";

                if (inversion) command.append(method_parameter);

                std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
                system(command.c_str());
                std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

                double time_diff = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();

                std::cout << "Time elapsed for N=" << dimension << ": "  << time_diff << "[ms]" << std::endl;
                times.push_back(time_diff);

                arma::mat output;
                output.load("output.csv");

                auto numerical_solution = output.col(0);
                double errors = 0;

                for(int i = 0; i < output.n_rows; i++){
                    errors += abs(numerical_solution(i)-BSCallPrice(x0+i*dx, 100, 0.1, 2, 0.0025));
                }

                errors /= output.n_rows;

                errors_vector.push_back(errors);

        }

        if(!inversion){
          export_to_csv("times.csv", dimensions, times);
          export_to_csv("errors.csv", dimensions, errors_vector);
        }
        else{
          export_to_csv("times_inversion.csv", dimensions, times);
          export_to_csv("errors_inversion.csv", dimensions, errors_vector);
        }

}
