#include "utils.h"

double BSCallPrice(double x, double strike, double vol, double tt, double r){
  double result;
  double d1 = (1/(vol*sqrt(tt)))*(log(x/strike)+tt*(r+0.5*vol*vol));
  double d2 = d1 - vol*sqrt(tt);

  result = norm_cdf(d1)*x-norm_cdf(d2)*strike*exp(-r*tt);
  return result;
}

//credits: https://www.quantstart.com/articles/European-vanilla-option-pricing-with-C-and-analytic-formulae/

double norm_cdf(const double x) {
    double k = 1.0/(1.0 + 0.2316419*x);
    double k_sum = k*(0.319381530 + k*(-0.356563782 + k*(1.781477937 + k*(-1.821255978 + 1.330274429*k))));

    if (x >= 0.0) {
        return (1.0 - (1.0/(pow(2*M_PI,0.5)))*exp(-0.5*x*x) * k_sum);
    } else {
        return 1.0 - norm_cdf(-x);
    }
}

void export_to_csv(string filename, vector<int> myvector, vector<double> times){

  ofstream out;
  out.open(filename);

  for(int i = 0; i < myvector.size(); i++){
    out << myvector[i] << "," << times[i];
    if(i != myvector.size()-1) out << endl;
  }

  out.close();

}


vector<int> convert_vec_string_to_vec_double(vector<string> input){

  vector<int> result;

  for(int i = 0; i < input.size(); i++){
    result.push_back(stoi(input[i]));
  }

  return result;
}


vector<string> split_string(const char* input, char delimiter){

  vector<string> result;

  int no_sizes = count_char(input, delimiter) + 1;
  string part = "";
  int i = 0;

  while(no_sizes > 0){

    if(i == strlen(input)){
      result.push_back(part);
      break;
    }

    if(input[i] == delimiter){
      result.push_back(part);
      no_sizes--;
      i++;
      part = "";
    }

    part += input[i];
    i++;

  }

  return result;
}

int count_char(const char* input, char character){
  int count = 0;
  for(int i = 0; i < strlen(input); i++){
    if(input[i] == character) count++;
  }

  return count;
}

void print_vector(vector<int> my_vector){
  for(int i = 0; i < my_vector.size(); i++){
    cout << my_vector[i] << endl;
  }
}
