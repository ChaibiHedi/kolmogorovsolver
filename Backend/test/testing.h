#ifndef TESTING_H
#define TESTING_H

#include <string>
#include <cstring>
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <armadillo>
#include <chrono>
#include "utils.h"
using namespace std;

void test(const char* sizes, const char* path, bool inversion = false);

#endif
