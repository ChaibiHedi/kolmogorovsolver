\contentsline {section}{\numberline {1}Background}{1}{section.1}%
\contentsline {section}{\numberline {2}Project Requirements}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Minimum Requirements}{2}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Stochastic partial differential equation solver (C++)}{2}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}Visualisation User Interface (Python)}{3}{subsubsection.2.1.2}%
\contentsline {subsection}{\numberline {2.2}Extension}{3}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Calculator Transformation (Python)}{4}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Two dimensional extension (C++)}{4}{subsubsection.2.2.2}%
\contentsline {subsection}{\numberline {2.3}System Boundaries}{4}{subsection.2.3}%
\contentsline {section}{\numberline {3}Development}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}Software development techniques}{5}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Version Control}{5}{subsection.3.2}%
\contentsline {section}{\numberline {4}Back-end (C++)}{5}{section.4}%
\contentsline {subsection}{\numberline {4.1}Research}{5}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Libraries}{6}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Armadillo Library}{6}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Exprtk Library}{6}{subsubsection.4.2.2}%
\contentsline {subsection}{\numberline {4.3}A Forward Equation Use Case: The Heat Equation}{7}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Analytical Solution}{7}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Numerical Solution}{8}{subsubsection.4.3.2}%
\contentsline {subsection}{\numberline {4.4}A Backward Equation Use Case: The Black Scholes Equation}{8}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Testing}{9}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Preparing for fusion}{11}{subsection.4.6}%
\contentsline {subsubsection}{\numberline {4.6.1}Command Line Arguments}{11}{subsubsection.4.6.1}%
\contentsline {subsubsection}{\numberline {4.6.2}Error Handling}{12}{subsubsection.4.6.2}%
\contentsline {section}{\numberline {5}Front-end (Python)}{13}{section.5}%
\contentsline {subsection}{\numberline {5.1}Animation}{13}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}GUI}{13}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Methodology}{14}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}Graph the data}{14}{subsubsection.5.3.1}%
\contentsline {subsubsection}{\numberline {5.3.2}Design the GUI}{16}{subsubsection.5.3.2}%
\contentsline {subsubsection}{\numberline {5.3.3}Testing}{19}{subsubsection.5.3.3}%
\contentsline {section}{\numberline {6}Final Program}{20}{section.6}%
\contentsline {subsection}{\numberline {6.1}PyInstaller}{20}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Doxygen}{21}{subsection.6.2}%
\contentsline {section}{\numberline {7}Group Work}{21}{section.7}%
\contentsline {subsection}{\numberline {7.1}Back-end}{21}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Front-end}{21}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}Schedule}{21}{subsection.7.3}%
\contentsline {section}{\numberline {8}Final Product}{22}{section.8}%
\contentsline {section}{Appendices}{25}{section*.11}%
\contentsline {section}{\numberline {Appendix A}}{25}{appendix.A}%
\contentsline {section}{\numberline {Appendix B}}{25}{appendix.B}%
\contentsline {section}{\numberline {Appendix C}}{26}{appendix.C}%
\contentsline {section}{\numberline {Appendix D}}{26}{appendix.D}%
\contentsline {section}{\numberline {Appendix E}}{27}{appendix.E}%
\contentsline {section}{\numberline {Appendix F}}{27}{appendix.F}%
\contentsline {section}{\numberline {Appendix G}}{27}{appendix.G}%
\contentsline {section}{\numberline {Appendix H}}{27}{appendix.H}%
