\documentclass[a4paper,11pt]{article}
\usepackage[margin=2cm]{geometry}

\usepackage{fancyhdr}
\usepackage{tabularx}
\usepackage{hhline}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{appendix}
\usepackage{caption}
\usepackage[section]{placeins}
\usepackage{anyfontsize}

\pagestyle{fancyplain}
\fancyhf{}
\lhead{\fancyplain{}{M.Sc.\ Group Project Report}}
\rhead{\fancyplain{}{\today}}
\cfoot{\fancyplain{}{\thepage}}

\title{A Visualisation tool for Forward/Backward Kolmogorov Equations\\\Large{--- Report Two ---}}
\author{Hedi Chaibi, Lucia Simkanin, Omolara Lebard, Sami Khan\\
       \{hedi.chaibi19, lucia.simkaninova19, omolara.lebard19, sami.khan19\}@imperial.ac.uk\\ \\
       \small{Supervisor: Dr.\ Panos Parpas}\\
       \small{Course: CO530, Imperial College London}
}

\begin{document}
\maketitle

\section{Abstract}

This report summarises the work that has been carried out on creating a tool for solving and visualising forward and backward Kolmogorov equations. This is the second report which will discuss the work carried out between 01/02/2020 and 26/02/2020. Please refer to Report One to see the original strategy. The following will discuss any changes to the original specification, testing strategy and provide a revised schedule for the remainder of the project.

\section{Project Progress}

\subsection{Updated Specification}

At first glance it seemed that only the backward Kolmogorov equation needed to be solved due to lack of knowledge of the subject, however after reading the books mentioned in the previous report the context of the forward Kolmogorov equation was brought to attention. In the subsequent meeting with the supervisor he affirmed that the requirements were indeed to solve both forward and backward equations. The explanation of both equations are given below:

\subsubsection{Backward equation}
\normalsize
\begin{equation}
\frac{\partial}{\partial t}F(t,x)+\mu(t,x)\frac{\partial}{\partial x}F(t,x)+\frac{1}{2}\sigma^2(t,x)\frac{\partial^2}{\partial x^2}F(t,x) = 0\label{eq:1}
\end{equation}
subject to the final condition $F(T,x)=\phi(x)$\\
To discretize, the following substitutions are made:\\
$\frac{\partial F_{i,j}}{\partial t}\approx \frac{F_{i,j}-F_{i-1,j}}{\delta t}$ \hfill $\frac{\partial F_{i,j}}{\partial x}\approx \frac{F_{i,j+1}-F_{i,j-1}}{2\delta x}$ \hfill
$\frac{\partial^2 F_{i,j}}{\partial x^2}\approx \frac{F_{i,j+1}-2F_{i,j}+F_{i,j-1}}{\delta x^2}$\\
The following recursion is then used to fill the matrix of $F(t,x)$:\\
$F_{i-1,j}=F_{i,j}+\frac{\delta t}{2\delta x}\mu_{i,j}\left(F_{i,j+1}-F_{i,j-1}\right)+\frac{\delta t}{2\delta x^2}\sigma_{i,j}^2\left(F_{i,j+1}-2F_{i,j}+F_{i,j-1}\right)$

\subsubsection{Forward equation}
\normalsize
\begin{equation}
\frac{\partial}{\partial t}F(t,x)+\frac{\partial}{\partial x}\left[\mu(t,x)F(t,x)\right]-\frac{1}{2}\frac{\partial^2}{\partial x^2}\left[\sigma^2(t,x)F(t,x)\right] = 0\label{eq:1}
\end{equation}
subject to the initial condition $F(0,x)=\phi(x)$\\
To discretize, the following substitutions are made:\\
$\frac{\partial F_{i,j}}{\partial t}\approx \frac{F_{i+1,j}-F_{i,j}}{\delta t}$ \hfill $\frac{\partial \mu_{i,j}F_{i,j}}{\partial x}\approx \frac{\mu_{i,j+1}F_{i,j+1}-\mu_{i,j-1}F_{i,j-1}}{2\delta x}$ \hfill
$\frac{\partial^2 \sigma_{i,j}^2F_{i,j}}{\partial x^2}\approx \frac{\sigma_{i,j+1}^2F_{i,j+1}-2\sigma_{i,j}^2F_{i,j}+\sigma_{i,j-1}^2F_{i,j-1}}{\delta x^2}$\\
The following recursion is then used to fill the matrix of $F(t,x)$:\\
$F_{i+1,j}=F_{i,j}-\frac{\delta t}{2\delta x}\left(\mu_{i,j+1}F_{i,j+1}-\mu_{i,j-1}F_{i,j-1}\right)+\frac{\delta t}{2\delta x^2}\left(\sigma_{i,j+1}^2F_{i,j+1}-2\sigma_{i,j}^2F_{i,j}+\sigma_{i,j-1}^2F_{i,j-1}\right)$\\~\\
\footnotesize{Note: $F_{i,j} = F(t_i,x_j)$, $\mu_{i,j} = \mu(t_i,x_j)$ and $\sigma_{i,j}^2 = \sigma(t_i,x_j)^2$}
\subsection{Current Progress}

\subsubsection{Front End}
\normalsize
In the initial stages, the interaction with back end is performed via a \texttt{csv} file. As the front end was being developed at the same time, randomly generated dummy data was used to successfully produce a graph by the indicated deadline. Afterwards, the graph was animated to display changes based on the time variable. The change of output from the back end was made part-way through the implementation as shown in \ref{AppA} and discussed later. The execution of this change was fairly straightforward, involving only one variable being read from the whole file, in comparison to previously reading three. As a consequence, the amount of code needed to implement this has decreased in length and therefore increased readability. Additionally as both time and space variables are in an interval they were able to be initiated using a \textit{numpy} array which made the program no longer dependent on the file past that point in the code. This makes testing changes in the two variables considerably simpler as a new \texttt{csv} file does not need to be generated each time. All the above was completed by the deadlines described in report one.

Further steps will include designing a simple GUI with a slider that will allow the user to manipulate the time variable and seeing applicable changes in real time. This should be achieved by the indicated date 07/03/2020 without any foreseeable delays.

The libraries used in the program so far are:\\
\texttt{numpy} - for storing the array from the file\\
\texttt{matplotlib} - for plotting and animating the data in a line graph\\
\texttt{imagemagick} - for saving the output as a \texttt{.gif}

\subsubsection{Back End}
\normalsize
The first week since the first report was spent researching the method of finite differences as well as the different types of equations that can be solved using this method. This took a long time as there was many different ways in which to make the functions discrete. The three main methods were \textit{Explicit method}, \textit{Implicit method} and \textit{Crank-Nicholson method}. It was then found that all the methods were necessary in making all the variables discrete for both forward and backward equations.

After the research, the coding started by firstly using a solution to the general heat equation system shown in \ref{AppB}, which was of the form of a Gaussian and decayed over time. This was done in order to test the different method of storing the data in the \texttt{csv} file. The initial thought was to store the data in three columns (shown in \ref{AppA}) \{time, spatial value, function value\} so that every possible combination of time and spatial value in their respective ranges is included. From having coded the solution, it became clear that it was much easier and more efficient to represent the output as a matrix (i.e. exactly how it was represented in the program itself). The output \texttt{csv} file was passed to the front-end developers who (after making some changes to their program from the original plan to match the matrix layout) were able to plot the graph of these results and later an animation as described in the section above.

Once the aim was clear in terms of output, the programming a general backward Kolmgorov equation began, having defined the parameters as well as the functions $\mu$ and $\sigma$. Calculations were carried out by hand to calculate the recursions that were required (shown in Section 2.1), using substitutions to discretize the function. Using the boundary condition the first row of the matrix $F$ was filled and then using the recursion and the first row the rest of the rows were filled. This was then repeated for the forward equation. This example is formulated and graphed in \ref{AppC}

The libraries used in the program so far are:\\
\texttt{vector} - for storing the matrices used in the calculations\\
\texttt{cmath} - for the mathematical functions/constants used in the calculations\\
\texttt{fstream} - for output to file

\subsubsection{Version Control}
\normalsize
The whole group has been keeping up with each others work by checking the \textit{Git} repository frequently. Two main branches were created at the beginning; one for the front end and one for the back end. These branches are often merged together once a milestone is reached, in order to let the other development team (front end or back end) can test the update. This report was also on \textit{GitLab} so that everyone can add their input directly into the report and push like with any other piece of code. Further to this, all group members have been updating their own log which includes the commits that they worked on.

\section{Testing Strategy}

\subsection{Front End}

Firstly, manual testing was performed to identify any potential undesirable effects. After such scenarios were identified and accounted for, tests were written down with the help of unit testing framework \texttt{unittest}. Following this, coverage testing was performed using \texttt{coverage.py} integrated within the IDE \textit{PyCharm}. The results indicate 100\% coverage with all lines covered, as can be seen in the table in Figure \ref{covtab} (A screen shot of the actual output is provided in \ref{AppD}). However, it should be noted that reaching 100\% coverage does not indicate flawless program and more testing will be performed in the future. This will consist of similar steps as described above, specifically making sure that additional possible input variables will not break the front end code, as currently there is no direct interaction with back end. Once the stage with direct interaction (which is an extension to the project) is reached, testing methodology will expand to include more diverse testing strategies.

\begin{figure}[ht]
\center
\begin{tabular}{lrrrr}
\textit{Module} & \textit{Statements} & \textit{Missing} & \textit{Excluded} & \textit{Coverage}\\
\hline\hline
\texttt{python\_graph.py} & 46 & 0 & 0 & 100\%\\
\hline
\texttt{test\_python\_graph.py} & 46 & 0 & 0 & 100\%\\
\hline
\textbf{Total} & \textbf{92} & \textbf{0} & \textbf{0} & \textbf{100\%}
\end{tabular}
\caption{Coverage Report for front end files: 100\%}
\label{covtab}
\end{figure}
\FloatBarrier

\subsection{Back End}

The supervisor of this project has provided just one equation (to date) to be tested. In the next week he should have more equations, which can be tested using the program. A similar equation to the one provided was also found on-line. However, it is very difficult to find more diverse equations along with their boundary conditions and the joint solution as many of these equations do not have easily deductible analytic solutions (hence the need for this program to calculate the numeric solution).

The results of the coverage test in Figure \ref{covtabb} show 100\% coverage of all the files involved in the program (A screen shot of the actual output is provided in \ref{AppE}). The test was carried out using a \texttt{main.cpp} file which contained an instance of both a forward and a backward equation together with the built in coverage tool in \textit{CLion} IDE for C++ applications. Hits in the table below refer to how many times each function was visited in the run of the program.

\begin{figure}[ht]
\center
\begin{tabular}{llrr}
\textit{File} & \textit{Function} & \textit{Hits} & \textit{Coverage}\\
\hline\hline
\texttt{utils.cpp} & & & 100\%\\
\hline
& \texttt{print\_vector\_matrix} & 2 &\\
\hline
\texttt{KolmogorovEquation.h} & & & 100\%\\
\hline
& \texttt{Forward} - constructor & 1 &\\
& \texttt{Backward} - constructor & 1 &\\
\hline
\texttt{KolmogorovEquation.cpp} & & & 100\%\\
\hline
& \texttt{Kolmogorov} - constructor & 34 &\\
& \texttt{Forward::finite\_difference} & 101 &\\
& \texttt{Backward::finite\_difference} & 11 &\\
& \texttt{mu} & 2242 &\\
& \texttt{sigma} & 2242 &\\
& \texttt{boundary\_condition} & 30 &\\
\hline
\texttt{main.cpp} & & & 100\%\\
\hline
& \texttt{main} & 1 &\\
\hline
\textbf{Total} & & & \textbf{100\%}
\end{tabular}
\caption{Coverage Report for back end files: 100\%}
\label{covtabb}
\end{figure}
\FloatBarrier

\subsection{System Testing}

Currently the only link between the back end and the front end is the \texttt{csv} file, that is why, to date, it would be futile to test the system as a whole and is better tested in parts. In the future, the \textit{Python} program should call the \textit{C++} program itself and then there will be a system to test all together.

It is important at this stage to explain the plan on testing the accuracy of the solution of the program as the output is mathematical in nature. The matrix of the numerical solution ($F$) will be compared with the matrix of the analytic solution ($f$) by using the following metric:
$$\text{error}_i = \sum\limits_{j = 1}^{\text{number of columns}}\frac{F_{ij}-f_{ij}}{|f_{ij}|}$$ 
This metric will calculate the relative error at each time instance by summing along the rows. This can be taken further by calculating the total error by summing $error_i$ over the rows. If the error value is negative then the numeric solution undershoots the actual solution, likewise if the solution is positive then the numeric solution overshoots the actual solution. The closer the error is to 0 (negative or positive) the closer the numeric solution is to the actual solution.

\section{Revised Schedule}

Having applied Hofstadter's law iteratively, the previous schedule was created, however the timing still fell short. The group underestimated the time it would take to get through the conceptual understanding by a full week (finished on 04/02/2020). Although this delayed the backend, it did not affect the front end (\textit{Graph the data}) and the development continued and was finished well before the deadline. Once the conceptual understanding was done, the back end development team started to develop the \textit{C++} code for the forward and backward equation for which a working prototype was fully rendered on 17/02/202, although this does not match the date on the schedule it took the same amount of time as planned. The front end concurrently started working on the graphical user interface which is currently in progress. The testing was done over the final three days before the date of writing this report. It is now realised that another slot will need to be created for testing as more equations are provided/found to test on. The new schedule is given in \ref{AppF}.

\newpage
\appendix
\appendixpage
\addappheadtotoc
\appendix
\renewcommand{\thesection}{Appendix \Alph{section}}

\section{}
\label{AppA}

\begin{figure}[ht]
\center\begin{tabular}{|c|c|c|}
\hline
$t$ & $x$ & $F(t,x)$\\
\hhline{|=|=|=|}
0 & -1 & 0\\
\hline
0 & 0 & 1\\
\hline
0 & 1 & 0\\
\hline
1 & -1 & 0\\
\hline
1 & 0 & 0.5\\
\hline
1 & 1 & 0\\
\hline
$\vdots$ & $\vdots$ & $\vdots$\\
\end{tabular}\hspace*{5cm}
\Huge$\left(\begin{array}{ccc}
0 & 1 & 0\\
0 & 0.5 & 0\\
\vdots & \vdots & \vdots
\end{array}\right)$
\normalsize
\caption*{The initial format of the \texttt{csv} file (left) and the re-imagined version of the \texttt{csv} file (right)}
\end{figure}

\section{}
\label{AppB}
\begin{figure}[ht]
\center
\begin{equation*}
\begin{aligned}
\frac{\partial u}{\partial t}&=\frac{1}{2}\frac{\partial^2 u}{\partial x^2} \text{ for } -5 \leq x \leq 5, 0\leq t\leq 10\\
\text{Initial Condition: } u(0,x) &= \delta(x) = \left\{\begin{array}{ll}
1 & x = 0,\\
0 & \text{otherwise}.
\end{array}\right.\\
\text{Solution: } u(t,x) &= \frac{1}{\sqrt{\pi t}}\exp\left(\frac{x^2}{2t}\right)
\end{aligned}
\end{equation*}
\caption*{1D Heat equation and its analytic solution, note this is forward equation with $\mu = 0$ and $\sigma = 1$}
\end{figure}

\section{}
\label{AppC}
\begin{figure}[ht]
\center
\includegraphics[scale=0.5]{anim.png}
\caption*{A still of the animation produced by the analytic solution in Appendix \ref{AppB} at time $t = 0$}
\end{figure}

\newpage
\section{}
\label{AppD}
\begin{figure}[ht]
\center
\includegraphics[scale=0.4]{some_table.jpeg}
\caption*{The coverage report as seen on \textit{PyCharm} for the front end}
\end{figure}

\section{}
\label{AppE}
\begin{figure}[ht]
\center
\includegraphics[scale=0.6]{some_other_table.jpeg}
\caption*{The coverage report as seen on \textit{CLion} for the back end, note both files \texttt{stochastic.cpp} and \texttt{stochastic.h} are empty}
\end{figure}

\section{}
\label{AppF}
\begin{figure}[ht]
\fontsize{8}{9}\selectfont
\begin{tabularx}{\linewidth}{|X|X|c|c|c|}
\hline
Back End & Front End & Previous Deadline & Completed On & New Deadline\\
\hhline{|=|=|=|=|=|}
Conceptual understanding & & 29/01/2020 & 04/02/2020 & -\\
\hline
\multicolumn{2}{|c|}{First Report} & 31/01/2020 & 31/01/2020 & -\\
\hline
 & Graph the data & 31/01/2020 & 30/01/2020 & -\\
\hline
Develop numerical solution for PDEs & & 10/02/2020 & 17/02/2020 & -\\
\hline
\multicolumn{2}{|c|}{Tests} & 17/02/2020 & 25/02/2020 & -\\
\hline
\multicolumn{2}{|c|}{Second Report} & 21/02/2020 & 26/02/2020 & 26/02/2020\\
\hline
\multicolumn{2}{|c|}{Final Overall Tests} & 17/02/2020 & - & 03/03/2020\\
\hline
 & Design the graphical UI & 07/03/2020 & - & 07/03/2020\\
\hline
\multicolumn{2}{|c|}{Minimum requirements completion} & 13/03/2020 & - & 13/03/2020\\
\hline
Allow user to change parameters through command-line & & 28/03/2020 & - & 28/03/2020\\
\hline
Create link to pass values from Python to C++ & & 10/04/2020 & - & 10/04/2020\\
\hline
Implement the changes in the calculation & & 25/04/2020 & - & 25/04/2020\\
\hline
 & Plot the data on a 3D graph & 06/05/2020 & - & 06/05/2020\\
\hline
\multicolumn{2}{|c|}{Third Report} & 10/05/2020 & - & 10/05/2020\\
\hline
\multicolumn{2}{|c|}{Final Submission} & 13/05/2020 & - & 13/05/2020\\
\hline
\end{tabularx}
\caption*{The updated schedule}
\end{figure}

\end{document}
