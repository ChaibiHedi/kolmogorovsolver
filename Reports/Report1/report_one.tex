\documentclass[a4paper,11pt]{article}
\usepackage[margin=2cm]{geometry}

\usepackage{fancyhdr}
\usepackage{tabularx}
\usepackage{hhline}
\usepackage{amsmath}
\usepackage{graphicx}

\pagestyle{fancyplain}
\fancyhf{}
\lhead{\fancyplain{}{M.Sc.\ Group Project Report}}
\rhead{\fancyplain{}{\today}}
\cfoot{\fancyplain{}{\thepage}}

\title{A Visualisation tool for Forward/Backward Kolomogrov Equations\\\Large{--- Report One ---}}
\author{Hedi Chaibi, Lucia Simkanin, Omolara Lebard, Sami Khan\\
       \{hedi.chaibi19, lucia.simkaninova19, omolara.lebard19, sami.khan19\}@imperial.ac.uk\\ \\
       \small{Supervisor: Dr.\ Panos Parpas}\\
       \small{Course: CO530, Imperial College London}
}

\begin{document}
\maketitle

\section{Project Specifications}

\subsection{Abstract}

Stochastic Partial Differential Equations (PDE) are part of an elegant subject that connects topics from stochastic calculus, probability theory and partial differential equations. The applications are numerous, spanning from the study of heat equations to the pricing of complex financial derivatives. One way of numerically solving these PDEs is by using the method of finite differences, which transforms the equation into a set of linear discrete equations. These can be solved using matrix algebra and therefore makes the method particularly suited for modern computers. The software this project aims at creating should make use of these approaches to enable the user to visualise the time evolution of the probability density function of a stochastic variable described by a PDE. Drift and diffusion terms as well as boundary conditions are specified by the user.

\subsection{Background}

The one dimensional equation we are attempting to solve is:
\begin{equation}
\frac{\partial F}{\partial t}(t,x)+\mu(t,x)\frac{\partial F}{\partial x}+\frac{1}{2}\sigma^2(t,x)\frac{\partial^2F}{\partial x^2}(t,x) = 0\label{eq:1}
\end{equation}
with boundary condition:
\begin{equation}
F(T,x)=\phi(x)
\end{equation}
Here the variables are:\\
$F(t,x)$ - the function we need to find, this can represent any stochastic variable.\\
$\mu(t,x)$ - the diffusion term.\\
$\sigma(t,x)$ - the drift term.\\
$\phi(x)$ - the boundary condition at time $t=T$.\\~\\
We make the function discrete by using the following substitution into \eqref{eq:1}:
\begin{equation}
\frac{\partial F}{\partial t}(t_i,x_j)\approx\frac{1}{\delta t}(F(t_i,x_j) - F(t_{i-1},x_j)),\quad\frac{\partial F}{\partial t}(t_i,x_j)\approx\frac{1}{\delta x}(F(t_i,x_{j+1}) - F(t_i,x_j))
\end{equation}
Then we use the method of finite differences to find the function $F(t,x)$.

\subsection{Minimum Requirements}

Our primary goal is to build a stochastic partial differential equation solver using \textit{C++} and enable the visualisation of the time evolution of the corresponding probability density function using \textit{Python}. The equations should be solved in one-dimensional space only. The visualisation can be done either as an animation or through graphical user interface. Initially, the back end and front end should interact via a file and the parameters for the equations will be entered manually through the \textit{C++} source code file.

\begin{figure}[ht]
\center
\includegraphics[scale=0.5]{minimum_graph.png}
\caption{A prototype of the output from the python program}
\end{figure}

\subsection{Extensions}

The main extension for this project will be to make the application interactive. Through an intuitive interface, the user is able to specify the boundary conditions of the stochastic differential equation, the drift function and the diffusion function. In this way, the user will have no interaction with the back-end part of the software, only needing to input the system’s specifications.\\
To further extend the scope of the project, the software could also be able to deal with stochastic differential equations that involve two-dimensional Brownian Motion. 

\begin{figure}[ht]
\center
\includegraphics[scale=0.5]{extra_graph.png}
\caption{A prototype of the output from the extended python program}
\end{figure}

\section{Project Requirements}

\subsection{Stochastic partial differential equation solver (C++)}

\begin{tabularx}{\linewidth}{|X|ccc|}
\hline
Requirement & Functionality & Risk Level & Necessity\\
\hhline{|=|===|}
Improve conceptual understanding of the project\hspace{1cm}
Books Used: \cite{klebaner2005introduction}\cite{allaire2007numerical}\cite{bjork2009arbitrage}\cite{mikosch1998elementary} & Non-Functional & Low & Essential\\
\hline
Solve stochastic PDEs using finite differences method & Functional & High & Essential\\
\hline
Output the solution to a file & Functional & Low & Essential\\
\hline
\end{tabularx}

\subsection{Visualisation User Interface (Python)}

\begin{tabularx}{\linewidth}{|X|ccc|}
\hline
Requirement & Functionality & Risk Level & Necessity\\
\hhline{|=|===|}
Load data from the appropriate file & Functional & Low & Essential\\
\hline
Plot the data on the graph (solution against spatial parameters) & Functional & Moderate & Essential\\
\hline
Combine individual snapshots at set time by incrementing the time value & Functional & High & Essential\\
\hline
Identify a dataset for testing purposes & Non-Functional & Low & Essential\\
\hline
\end{tabularx}

\subsection{Calculator Transformation (Python)}

\begin{tabularx}{\linewidth}{|X|ccc|}
\hline
Requirement & Functionality & Risk Level & Necessity\\
\hhline{|=|===|}
Design the graphical user interface & Non-Functional & Low & Non-Essential\\
\hline
Allow the user to change parameters through command-line & Functional & Moderate & Non-Essential\\
\hline
Create link to pass values from Python to C++ & Functional & High & Non-Essential\\
\hline
\end{tabularx}

\subsection{Two dimensional extension (C++)}

\begin{tabularx}{\linewidth}{|X|ccc|}
\hline
Requirement & Functionality & Risk Level & Necessity\\
\hhline{|=|===|}
Research differences in solving 2D equations & Non-Functional & Low & Non-Essential\\
\hline
Implement the changes in the calculation & Functional & High & Non-Essential\\
\hline
Plot the data on a 3D graph & Functional & High & Non-Essential\\
\hline
\end{tabularx}

\section{System Boundaries}

We will use the \textit{Linux} operating system to perform all of the coding, as this is free and open source and hence can easily and efficiently be tested by our supervisor. The \textit{Linux} environment in particular will be \textit{Ubuntu 18.04 Linux} which for us can be accessed directly through Lab machines or through a \textit{ssh} link.\\
The choice of using \textit{C++} to write the backend of the software has come after discussions with our supervisor as well as internet research which all points to the fact that we are bound to get higher performance if we perform the calculations on \textit{C++} rather than on \textit{Python}. This is because \textit{C++} is a compiled language and contains primitives while \textit{Python} does not.\\
However, we have chosen to program the front end in \textit{Python 3.6} as there are many built in libraries that are commonly used in industry (such as \textit{matplotlib}, \textit{numpy},...) which makes graphing and creating a User-Interface (UI) very simple. We also discussed the use of \textit{Javascript} for the front end as it is much simpler to get better looking UIs, but our supervisor preferred us to use \textit{Python} as he was more comfortable with it.

\section{Version Control}

We will be using the \textit{DoC GitLab} throughout the project. The main reasoning includes the ease of concurrent access and simplistic and forced communication through commits. Additionally, the connection between \textit{Linux} and \textit{Git} allows for an easy way to push/commit/merge files to which we are all already accustomed to.

\section{Development Strategy}

\subsection{Milestones}

\begin{tabularx}{\linewidth}{|X|X|c|}
\hline
Back End & Front End & Deadline\\
\hhline{|=|=|=|}
Conceptual understanding & & 29/01/2020\\
\hline
\multicolumn{2}{|c|}{First Report} & 31/01/2020\\
\hline
 & Graph the data & 31/01/2020\\
\hline
Develop numerical solution for PDEs & & 10/02/2020\\
\hline
Final Tests & & 17/02/2020\\
\hline
\multicolumn{2}{|c|}{Second Report} & 21/02/2020\\
\hline
\multicolumn{2}{|c|}{Final Overall Tests} & 17/02/2020\\
\hline
 & Design the graphical UI & 07/03/2020\\
\hline
\multicolumn{2}{|c|}{Minimum requirements completion} & 13/03/2020\\
\hline
Allow user to change parameters through command-line & & 28/03/2020\\
\hline
Create link to pass values from Python to C++ & & 10/04/2020\\
\hline
Implement the changes in the calculation & & 25/04/2020\\
\hline
 & Plot the data on a 3D graph & 06/05/2020\\
\hline
\multicolumn{2}{|c|}{Third Report} & 10/05/2020\\
\hline
\multicolumn{2}{|c|}{Final Submission} & 13/05/2020\\
\hline
\end{tabularx}

\subsection{Strategy}

The project will be managed with the aid of the online project management software \textit{Workast} which is running as an add-on to \textit{Slack}. An adapted scrum approach will be used, with daily online meetings assisted by \textit{Slack}.
The meetings to update the team on tasks will be carried out with the aid of the \textit{Standuply} slack extension and will only include the team members. Additionally, pair-programming will be implemented as needed.

Furthermore, each week a meeting with the supervisor will take place on Friday 9.30 AM. Hedi and Sami will take primary responsibility for the back-end part of the project (PDE solver), while Lucia and Omolara will be responsible for the front end (visualisation). However, all members will have an understanding of all areas of the project and will be included in essential discussions concerning both front end and back end. It should be noted that there are only four members in this group. The front and back end will be developed in tandem, as can be seen under the delivery timelines. Refactoring will be carried out at multiple steps after initial code design.

\bibliographystyle{plain}
\bibliography{report_one}
\end{document}
