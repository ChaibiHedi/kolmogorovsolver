##
# @file main.py
# @authors Lucia Simkanin, Omolara Lebard
# @date 20/01/2020
# @brief File containing methods for gui of the software
#

import numpy as np
from tkinter import *
import tkinter.font as tkFont
import tkinter.messagebox as tmb
from functools import partial
import matplotlib
import matplotlib.pyplot as plt
import subprocess as s
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import sys

## This creates labels (text) in the gui that supplies information to the user
class Label_class:
    ## The constructor
    #
    #  @param label_text is the text displayed in the label
    #  @param x_location is the x-grid location of the label
    #  @param y_location is the y-grid location of the label
    #  @param font_size is the font size
    #  @param col_span indicates how many columns the label should span in the grid
    #  @param location- indicates which object the label is placed in - e.g. frame or gui window
    def __init__(self, label_text, x_location, y_location, font_size, col_span, location):
        self.fontStyle = tkFont.Font(size=font_size)
        self.label = Label(location, text=f"{label_text}", font=self.fontStyle, justify=CENTER, anchor=CENTER, padx=10, bg='white')
        self.label.grid(row=y_location, column=x_location, columnspan=col_span, sticky=E+W)

## This creates entry-boxes that allows the user to input values
class Entry_boxes:
    ## The constructor
    #
    #  @param variable_name_text is the text for the corresponding label of the  entry box
    #  @param x_loc is the x-grid location of the entry box
    #  @param y_loc is the y-grid location of the entry box
    #  @param col_span indicates how many columns the label should span in the grid
    #  @param actual_variable is the variable to which the user-input is assigned.
    #  @param data_type is a string stating the type of data that should be input in the entry-box
    #  @param width is the width of the entry box
    #  @param location- indicates which object the label is placed in - e.g. frame or gui window
    def __init__(self, variable_name_text, x_loc, y_loc, col_span, actual_variable, data_type, width, location):
        self.entry = Entry(location, fg="black", bg="peach puff", width=width*8, textvariable=actual_variable, justify='center')
        self.entry.grid(row=y_loc, column=x_loc, columnspan=col_span, padx=(10,0), pady=(0,5))
        self.Box_label = Label_class(variable_name_text, x_loc, y_loc-1, 10, col_span, location)
        self.data_type = data_type

    ## Retrieves values entered into the entry-boxes in the gui.
    def get_value(self):
        if self.data_type == "float":
            return float(self.entry.get())
        else:
            return self.entry.get()

## Responsible for creating and displaying the GUI and all of its features
class tk_window:
    ## The constructor
    def __init__(self):
        self.root = Tk()
        self.root.title("Graph GUI")
        self.root.geometry('1200x800')
        self.root.configure(bg='white')

        self.selected = StringVar()
        self.methods = StringVar()
        self.x_min = StringVar()
        self.x_max = StringVar()
        self.dx = StringVar()
        self.t_min = StringVar()
        self.t_max = StringVar()
        self.dt = StringVar()
        self.f = StringVar()
        self.g = StringVar()
        self.h = StringVar()
        self.bc = StringVar()
        self.bc_top = StringVar()
        self.bc_bottom = StringVar()
        self.grapher = 0
        
        self.title_label = Label_class("KOLMOGOROV EQUATION SOLVER", 0, 0, 16, 2, self.root)

        self.entry_frame = Frame(self.root)
        self.entry_frame.grid(row=1, column=0, rowspan=4, sticky=N+S, padx=30, pady=30)
        self.entry_frame.configure(bg='white')

        self.graph_frame = Frame(self.root)
        self.graph_frame.grid(row=1, column=1, rowspan=3, sticky=N+S+E+W, padx=30, pady=30)
        self.graph_frame.configure(bg='white')

        self.slider_frame = Frame(self.root)
        self.slider_frame.grid(row=4, column=1, sticky=E+W, padx=30, pady=30)
        self.slider_frame.configure(bg='white')
        
        self.boxes_label = Label_class("Enter values for the following variables:", 0, 1, 12, 3, self.entry_frame)
        self.fontStyle = tkFont.Font(size=12)
        self.btn = Button(self.entry_frame, text="CALCULATE GRAPH VALUES", bg="dark salmon", font = self.fontStyle, command=self.call_cpp_funct)
        self.btn.grid(row=19, column=1, sticky=E+W)

        self.radio_frame = Frame(self.entry_frame)
        self.radio_frame.grid(row=2, column=0, columnspan=3, sticky=E+W)
        self.radio_frame.configure(bg='white')

        #This is the equation image, used when converting into single executable
        image_string = b'R0lGODlhTwErAPcAAAAAAAMDAwQEBAUFBQYGBgcHBwgICAoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzk5OTo6Ojs7Ozw8PD09PT4+PkBAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fX5+fn9/f4CAgIGBgYKCgoODg4SEhIWFhYaGhoeHh4iIiImJiYqKiouLi4yMjI2NjY6Ojo+Pj5CQkJGRkZKSkpOTk5SUlJWVlZaWlpeXl5iYmJmZmZqampubm5ycnJ2dnZ6enp+fn6CgoKGhoaKioqOjo6SkpKWlpaampqenp6ioqKmpqaqqqqurq6ysrK2tra6urq+vr7CwsLGxsbKysrOzs7S0tLW1tba2tre3t7i4uLm5ubq6uru7u7y8vL29vb6+vr+/v8DAwMHBwcLCwsPDw8TExMXFxcbGxsfHx8jIyMnJycrKysvLy8zMzM3Nzc7Ozs/Pz9DQ0NHR0dLS0tPT09TU1NXV1dbW1tfX19jY2NnZ2dra2tvb29zc3N3d3d7e3t/f3+Dg4OHh4eLi4uPj4+Tk5OXl5ebm5ufn5+jo6Onp6erq6uvr6+zs7O3t7e7u7u/v7/Dw8PHx8fLy8vPz8/T09PX19fb29vf39/j4+Pn5+fr6+vv7+/z8/P39/f7+/v///wAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAABPASsAAAj+APMJHEiwoMGDCBMqXMiwocN7AvFBdEixosWLGDNq3Mixo0aJ+BLiC+mxpEmQ+ULCewUqXMqDI0manIkRpUiZNHPq3BlxYj57Bu/V6zmSp1GDRVNCdGcpmCshu34SFBry3j18QI9qvSrQHs6U9SZ65aq1rNmG2Zq1y+eTbUh76IC2PZuz6rRo9ARWWyLQUJCIArnaSzfP7Ve6HkOuY5btq0SB7dylxIq4MuJWoEYJ8vXycTdEgB45CpYPEjzLJ/OZCzWKkyBt+cIBKixqRt6q+bwlCvRoEbN2s16i5gjxmKZTkhoJh0gvlZ5FjT7ls0VN+PDrJYFOgiSQWY5mP0P+XgKCSRm4ZpZ2OMHOseigVwJLGbktUIgfqUM5/aiU7Fu1Qk2sVxh7F0GUzR+w5UOHHfnUAxQ3TLShSzbeqOKGC9dYR+CGFRVlxBr5xJPPIGHkU5geLEzDFkTkXLAKWxxaZOAFrjSYjw/KndaIGT3lg8cJ1UyWDzwTxGFjjA4N9cgB7AzlTAQZ5pMMBohMBtEbGsijIZJcKrTLNza2woNAmBjgzE9WAfVFLjB2yVBI7MTCDoxr3CFQLVV+845AkyBATD70XAXUDdUd5mZBEEUjy0veAOBJPttwcMaK9xQmSRpbHqopSbToYUg7qYhgDzcJzNEmSY9I06amCUH0Dij+eXByjxRs5GNKGt08Y8o5+XzDQBmADhRSFHmxqhBQ3TSiRzHaACDdGQWsIxxQo8CXlbGHhtTOFaakw04jQiSRDyMAMLMqWIZiC1gxTRyDTjmOFPBIPijYsEMJ8+bDBwDInJuPO3OpG1g+iVwRjjnXmALAKvAA8Ie/9AwlMKs01GFiPugAYGQHPGiZ7sQEhWQNCKpcDAoAkQQbUUhAVJApyAWFNIkK6sDoQgLVNAOAKUcCBnOXEEHywThSnYNyPgZw4Zaw91z7M0RH5NAgRJEoUIyJ9mQNVDghNLElPBLDHJI7ETA4T0gKrKeHAMZINVClP3MJ1Ak+TJ3PHyiwZQD+GW4L9OppH2+q2gR0NAgUDUSoPLA5JYxh3VCOqBi4m0CZ8gBnXEGwRz6EZADbRCEx00nfcROIzwhK05OXDBbns0MVaBYVjCb+dhRTyCUlBeMyFZASbDgfwDcXUE7UoNRjwgh/1OQZDTWIB1flNckKAjkDQC+AFoWPIui8bJFNSKWGEEjanwXUD1fko2UjTMDYiwHCrJiPMpkwX2BIX5F1EUlYJRVSPSSQTl62kIdMAaUXFcCcQK5xB6IFbCb2UwhOZAIRW5zgMeQYwYsEwgNMxeQemcBeBNszwoTYQ38EqYdM8BE2jpywaRKcXxGIIY1RRIFXj5EEDWaBjGygIhH++GtIBEPCDXTs6SXksEQ6vCfBkKCDD9LiXz4skQVpQAMQpnogjEYxA1YkIxunYIQ5mIg7jEAkF7yoHUOsEhJslGJLYwhENY4RhVDI7xxKoAMzmuGMPhyDjMJiiDeg0Y3woeMS5QDkQbgyDz6A6StAYUcxmMGMQrbDaTWZSgzPEYg1LGqF+UgHI9DQCJeUsCEQIcYXroCGl8hjDv3SYkOA4gs9+Awi0JBDHZJBuvC14xFkcIQ35MeTkESjcIoUST4o8aK5vKIMiuDVtUKCizXMoRe4sV0+WuEAGNQOD/GT5RrzIY0zrIV/IXEFGrTQBz/goRhncIlGIDKMSeKCjBP+xMlcRgiXN0EECZk4gxqkwglC9G2E91jDafIih0X5ZIJvilkE7bEOcQYykGJQxqrsB5FbYAFG6hBDIUG5spA5hiLqKBZC8nIEBvkEIqIQxEYvOr58tMFchRmEJU6VDzbk4E8QMQcRICCZUw6MF1CwhjfCQLvAsZAemIxIPSJWQnHwqIVByYc1LMCZ0+SjHUiY00DkYhUYHiQvteiAiEJCDSqoNDCqs6iwpqpCh4SEHHog2sdYKJEX5gMXSiNJWU/YP4Schgl5E0o++jAvnNiDqouMq0XyAJ4HFqUGpAmLK5tQyLFSyqwGGUo2QhCkkdQDCqaESBhM0D3V7akOfNn+SD1gwLN8+GIE5EjmTL4BO6wWBCi6eAGvFDuKj/72Lb4dSGHS0ALP8sBcRt0IRMTBBnB4ryjzmMhQpgCLzrBFRA1KrkDyooRaCOkaOoiqTtYQy4MMxRUEGOOAhqKKLGBSYvbQ0krzcQkVFCsvQehuXjgBgLZlpTBuoJ16IwqNFXTvHvFYwaMe2DStWfjCGNbaYcqam49mF4VvS4UVSIAIzhRmC4mAUUhGIQtIXMMVlkBF+FQRCRcYQRHmGsoWBiGVCmf4x0DWcFBgGA43gImwMfvXIlDhilP8CR5AeIZS8rEMV1TCFcSQBSGwsaV5YKIPFqhDKKKYj+e6JchoPiH+Uth4Dzf0KyxzyUshbDAKXwQCGMGCg518ogpXTOIasqDEKLLqi0nsgAWIGEaw9hAFgZzjA09AVD52AQ01jvMRJygMUFwgh15qBRxZ6BlCzLEEK5gjkSEJApvUfIlY5GO17cDCmA5yjmFsQBbiqBlEBHEExZXlrnIwZfjkwQRF3MMZBKhEPpphA25IBRyXCAc1XkCIYSTgkwWhhzv8EABliIM+SXjjgk0SB3M9EChukEGQJDGDlyTBjpodRSryAQcLrMMNMUCIOqohg0ucY4wQ4UW7bQuAeTuNMgaxBB74sIeGO7zh1n1LPuYwgqEMxQVg8HQ+qjGLXODi4yAPucj+Rf5I5W5DG9oABzGakI5raCMb8jzICXhMj5C8QwfxA4o3gjGUM2ghH85I0G/zUQsEOBsiIQnFDsLDDFp4fORQjzrIdSGLYcrEHt7IRja+YQwwFAMc2sCGOAiSlze4QCDg0IAuppOCRA5lFafIRzQkYAx7+AK8BRnKH3gEunwkwRApYUcvbvF0qY88F7b4I07OgY1tnBwMsAgHNrSxjQHBSBwEkE4+JFECgcRjB/AZyjhqAZQ9rIca1tgSRK5BAUUDJSTboAFQ8lCuXupuIM74RTB2z/tgACMYRZXKGkRg8Xy0INTqpQUdAuGH5jv/+dB/fh94iZtuuOH6cRCDCer+wAY3qEE5SWbHB1xd13Xs4GoQgYdYe9DMt/kEKImIdFZCMgsduEUUdwBE9Pe//z7woZ1+EAhz0DZIlw/qwAdq4AZwMAYwMAZx8AZn8AhD8Rjh4AGYIBCxcAJjBAsvUDNVURipYAE4FBHTJBA4oGwDEhJQcB+xcQh88Af8F4N/kAdNRRChYAZwcH0wgAVx0AZt4AbOhhvEIAHbIBBaMAWQoQO0IBXzIFZGAAo9YxVvkw+ngAJzIhPsUAOFgQUhMHZ9Zw3BMW4LARSBUAKaZnytJIZBYVT1UA5uuA7OQCzi4IZLlGTckALDxBXwsAPh1BX5MAz8kg/v8FYF4QSF8FX+pwFTY7Jgt6dMU4YQ9nAO5UAO6hANZfAM60AOlEgSQ3ELBSYQbQAFAsELK5BbnJgPi/Bz+AAPUdVGALCENfcSS5BiyQU+FsEO5FAOp1YGucAO4+CGKgURmtBo+fUCS1gpPFAjWQER1AAAt5AP7GB5uCMHwOIOV5gP3SB7+QAIG+CFAyMKfUgQu2AKqYAK5niOqJAKS4QbsZACHkgPKWBQckUToCZqksYJ+TYZITEESwgU5bAoqGACkvEJGqVVqBYS5wAB82YLXEaG7UOIGGEOyiCNykQOwbYlQ4ELAZAgNQB4WoUDn7NsqYcCh8gOrjAn8BANU5gLxgMO0lEUSVD+MmrYEeVmaS6gNPkgChJAD8dQaUuwCTaiDrbwVyiwRKZwT/lwDURDEDKwCPnQC9gzFLkwcNAAAJyhQhDRCzu1T5SgB37QB2AJls3nSNbRDhoQP0npAYqWLlI0XbYQDB6zMpNzO9/wczXXiPiQF0/wA0eSF2NwH0ORCRRwDknQPtjwCJLBDh6wHlyxDRZADdRACWiSD1LAHQfHIrnwC3vClvnwDKoQDF/gal4BE48hDm9gXVcBSvWwA4tSCHSnFD8AHl+1AJTwDhKgHKXQXfmQCQnQCpPpCqKYCcdogpJjUhsnC86Qmt+jPW/AS17xFfVQAVCYD4FgBuZQCXlxB2/+IBWhUAHkcAWJ4w2LIC3x4AL2J1gjoArmoBwwxAZeYCN2IAMlhwqVEF1JVgaOkw8AQhEh8QuMcA668AVcNo+48w1SYI9vcz6AlxVAwQoHGhLCUAaBcA2TgAeM8GD5EAdfkEgTYQlsYApHBBQ7UJyBNAyUQA7AYAXgcRghEQ9ZoCKf0IWWdnlpEHHhIxuZUAgiwBz5oAVxFxKP4AapMA14UAiuBhHPgASSWVfjcAd98CdFEQ1DkBCZUArtIAluIF4UkQbtZRDsUCJAIQ1yMAjWlQ+yUAUDwgxmEAjSsAl2cAhjV3pWcJD5sApo0AljFyxBIEIhoQlJAAeLIAe3cDb+kFgPhnqoiApR84AHmYAKbtA9zAMR1fAEdegKPyAx4UAIwccQ5MAgC0YPqWcDlSZF9LAEueVZPzGanvcK1xgYgpUPwpBxSXYNSVCEf4UDNZUOVtA24AAANaKlCIkIqJYQCCYuj0FHy1EVr/cS3SAd04SVwfIGowNRpWAFFzMGcKBbB3EIqjKPMFQUT1BarpqqSWEPY2YdZHFXWjBGUyYUWaOta9RXBBoskEAA4DUOHLAo+YAJLyAkayRWWTUHRnALV3BSplBAy5quA+ENmXBQgDEUZpBGbRESvgABGlUYMBAVUvicjwEjoRAD9KEQ9wAw4bMONgAf2GAC5hVEcrD+dul6GK+gkqCEG9pQBsMaSHOABDbCDC0gF1Xxsw3RDlpqHW2ZD66wZ9ojRQJxSEQLGHlBg25ze424Ef5TQu/wDggjD+xAAo4gEF7wBRonI/mwCZAgCMtQO/dQCEOpRSFBCuy6SPngCYBnKPAQD+tgDTWDARknD5USPUgnWkLwR/MaPt/wAryADE7ACsIREt0QB5uaZNtQMgthCPfUiu8wD+OQDfUwDi0wTOHleYO7EYhwpAgBEacQc+GjfD7jJiEhC48wBr8QDQ3QCNJADCggBsdwTkIUUTChGq2wmSa0EObAHVokD7DgCGZQCe/gAUioDrwwDbkADsewDNWRD+L+AAbR0A7CthDM81jnckAuexOcqQ0804oGOAqA0AbGUAoh4GzYkAy90Ay58guQyr0fgTGpoLulGzjyQAnPmS0mUgWyOg9fUHDYcAgSoAq78LgfEUQ1tbv2Gz5mkAVaog2bsAGXkA+G4GoxgAXgMANdkA/rcAjV8QrKKD5lVEz5MA5MMG/5UAwe4AFZgwnbIA0fEAm+IAEvMpPtYVdvYp9mARGGQAOFkRdXMABnsgkwwMMQFLowoV5DQQgWkBKSIQYAkJSxEBI4YCS04GxcAAJM0MFkZhRS2B4BQxJE0Gj0UBg24AD5gAhnew0dYAzokAv6S8YewcTDYXMOczH5EAP+TRASZPBRdVU6U2gOIwAIbDFAD2AiKgQPLHC2VYEO5/AN4VAzhmwjpwAA1vUgHPAw7VAYk5BYmdwlIUEMB/AiQxEPD7BT8BADo+PEbjIUtQAAKgkU7ZAC0gFDnmABa1XKcEsHTHIxnwAAY5eX+SAGB1qAwLwhIZELLhAP9aAleGA8cjcCUrYKQZjJ71UAYHIakPACQ0EPY6cGU5oPpDDGzQwRbFAD9uAg+eAE27mKe3IDk9AgogCvzazC5ZAC2JMPwIADZzJpL/AOvzBvQMwhEDEOKcAm+aALLEAa+QAGOnAPOTApkbnPKZSTG4DJjuAEhTEOJfAKypAAwaEKS5i00BqdO/nwCmQQCYygBuYCFOfwByx2x4YMFK9wBovwCHGQRoWRCYLACq1wCLUACmOk0qb8xnrQCIdQB2LFDnBQCcFQCpBAC69AkSs9HBDRDbpADNJSgO6QDXqsLoz7C8IgIj7hDWP3DtQAvFs9EMzQC8mwrA2iDWsRDttQ1nGNx4HxUPij1EiSriVYWCCm0brzsimxjILd17bDRhLV1xIBYknb2JrCYSYV2I692Zzd2Z6NGgEBADs='


        self.photo = PhotoImage(data = image_string)
        self.photoimage=self.photo.zoom(1,1)
        self.image_label = Label(self.radio_frame, image=self.photoimage, width=400, height=60, bg='white')
        self.image_label.grid(row=0, column=0, columnspan=2)
        self.radio1 = Radiobutton(self.radio_frame, text='Forward Equation', value="1", borderwidth=1, variable=self.selected, bg='white', font=self.fontStyle)
        self.radio1.grid(row=1, column=0, padx=10, pady=5)
        self.radio2 = Radiobutton(self.radio_frame, text='Backward Equation', value="0", borderwidth=1, variable=self.selected, bg='white', font=self.fontStyle)
        self.radio2.grid(row=1, column=1, padx=10, pady=5)
        self.radio3 = Radiobutton(self.radio_frame, text='Thomas Algorithm', value="0", borderwidth=1,
                                  variable=self.methods, bg='white', font=self.fontStyle)
        self.radio3.grid(row=2, column=0, padx=10, pady=5)
        self.radio4 = Radiobutton(self.radio_frame, text='Inversion method', value="1", borderwidth=1,
                                  variable=self.methods, bg='white', font=self.fontStyle)
        self.radio4.grid(row=2, column=1, padx=10, pady=5)

        self.entry1 = Entry_boxes("Minimum x", 0, 4, 1, self.x_min, "float", 1, self.entry_frame)
        self.entry2 = Entry_boxes("Maximum x", 1, 4, 1, self.x_max, "float", 1, self.entry_frame)
        self.entry3 = Entry_boxes("dx", 2, 4, 1, self.dx, "float", 1, self.entry_frame)
        self.entry4 = Entry_boxes("Minimum t", 0, 6, 1, self.t_min, "float", 1, self.entry_frame)
        self.entry5 = Entry_boxes("Maximum t", 1, 6, 1, self.t_max, "float", 1, self.entry_frame)
        self.entry6 = Entry_boxes("dt", 2, 6, 1, self.dt, "float", 1, self.entry_frame)
        self.entry7 = Entry_boxes("f(x,t)", 0, 8, 3, self.f, "string", 6, self.entry_frame)
        self.entry8 = Entry_boxes("g(x,t)", 0, 10, 3, self.g, "string", 6, self.entry_frame)
        self.entry9 = Entry_boxes("h(x,t)", 0, 12, 3, self.h, "string", 6, self.entry_frame)
        self.entry10 = Entry_boxes("Boundary Condition", 0, 14, 3, self.bc, "string", 6, self.entry_frame)
        self.entry11 = Entry_boxes("Left boundary condition", 0, 16, 3, self.bc_top, "string", 6, self.entry_frame)
        self.entry12 = Entry_boxes("Right boundary condition", 0, 18, 3, self.bc_bottom, "string", 6, self.entry_frame)
        
        self.running = False
        self.current_index = 0
        self.max_index = 0
        self.root.protocol('WM_DELETE_WINDOW', self.close_app)
        self.canvas = None
        self.dummy = self.dummy_graph()

        self.root.columnconfigure(0, weight=0)
        self.root.columnconfigure(1, weight=1)
        self.root.rowconfigure(0, weight=0)
        self.root.rowconfigure(1, weight=1)
        self.root.rowconfigure(2, weight=1)
        self.root.rowconfigure(3, weight=1)

        for i in range(2):
            self.entry_frame.columnconfigure(i, weight=1)
        self.entry_frame.rowconfigure(0, weight=1)
        self.entry_frame.rowconfigure(19, weight=1)
        for i in range(1,19):
            self.entry_frame.rowconfigure(i, weight=0)

        self.graph_frame.rowconfigure(0, weight=1)
        self.graph_frame.columnconfigure(0, weight=1)

        for i in range(3):
            self.slider_frame.rowconfigure(i, weight=1)
        for i in range(2):
            self.slider_frame.columnconfigure(i, weight=1)

        for i in range(2):
            self.radio_frame.rowconfigure(i, weight=1)
            self.radio_frame.columnconfigure(i, weight=1)
        
        self.root.mainloop()

    ## This function is called when the slider is moved. It converts the slider output into an integer(the time index),
    # then uses the integer to call the update_graph function, 
    # which draws Kolmogorov equation graph for the time index provided)
    #
    #  @param event is the return value from the slider indicating that the slider position has been changed
    def draw_graph(self, event):
        convert = self.slider.get()
        index = int(convert)
        self.grapher.update_graph(index)

    ## Calls the backend program with several arguments entered by the user via entry boxes,
    #  creates a working slider and play and pause buttons, initialises the x and time variable,
    #  displays a graph generated based on the output of the backend program.
    def call_cpp_funct(self):
        if self.check_values():
            back_forward = self.selected.get()
            thomas_inverse = self.methods.get()
            x_minN = self.entry1.get_value()
            x_maxN = self.entry2.get_value()
            dxN = self.entry3.get_value()
            t_minN = self.entry4.get_value()
            t_maxN = self.entry5.get_value()
            dtN = self.entry6.get_value()
            f = self.entry7.get_value()
            g = self.entry8.get_value()
            h = self.entry9.get_value()
            bc = self.entry10.get_value()
            bc_top = self.entry11.get_value()
            bc_bottom = self.entry12.get_value()

            try:
                if thomas_inverse == 0:
                    backend = s.Popen(["./backend", str(back_forward), str(x_minN), str(x_maxN), str(t_minN), str(t_maxN), str(dxN), str(dtN), f, g, h, bc, bc_top, bc_bottom], stderr=s.PIPE)
                else:
                    backend = s.Popen(
                        ["./backend", str(back_forward), str(x_minN), str(x_maxN), str(t_minN), str(t_maxN), str(dxN), str(dtN), f, g, h, bc, bc_top, bc_bottom, str(thomas_inverse)], stderr=s.PIPE)
                output, error = backend.communicate()
                if not error:
                    self.canvas.get_tk_widget().grid_forget()
                    self.max_index = int((t_maxN - t_minN) / dtN)

                    self.slider = Scale(self.slider_frame, from_=0, to=self.max_index, length=400, width=30,
                                        orient=HORIZONTAL,
                                        showvalue=0, command=self.draw_graph, bg="dark salmon")  # returns an event
                    self.slider.grid(row=1, column=0, columnspan=2, sticky=E + W)
                    self.slider_label = Label_class("Move slider to view graph", 0, 0, 12, 2, self.slider_frame)

                    self.btn_play = Button(self.slider_frame, text="Play", bg="dark salmon", command=self.play)
                    self.btn_play.grid(row=2, column=0)
                    self.btn_pause = Button(self.slider_frame, text="Pause", bg="dark salmon", command=self.pause)
                    self.btn_pause.grid(row=2, column=1)
                    filename = "output.csv"
                    x = np.linspace(x_maxN, x_minN, int((x_maxN - x_minN) / dxN) + 1)
                    t = np.linspace(t_minN, t_maxN, int((t_maxN - t_minN) / dtN) + 1)
                    self.grapher = Grapher(filename, x, t)
                    self.grapher.read()
                    self.grapher.display_graph(self.graph_frame)
                else:
                    self.show_error(error)
            except Exception as message:
                self.show_error(message)
                sys.exit(1)

    ## Animates the graph and plays the animation from the current position of the slider,
    #  throughout the animation the slider moves along and after the animation finishes, the slider jumps to the
    #  initial position.
    def play(self):
        self.running = True
        self.current_index = int(self.slider.get())
        while self.running and self.current_index <= self.max_index:
            self.slider.set(self.current_index)
            self.root.update()
            self.current_index += 1

        if self.current_index > self.max_index:
            self.slider.set(0)
        self.running = False

    ## Pauses a running animation.
    def pause(self):
       self.running = False

    ## Creates and displays an empty graph, dummy buttons and a dummy slider.
    def dummy_graph(self):
        figure, axes = plt.subplots()
        axes.set_xlabel('x')
        axes.set_ylabel('p(x, t)')
        xvals = [0, 25, 50]
        yvals = [0, 0, 0]
        plotLine = axes.plot(xvals, yvals)
        plotTitle = axes.set_title('t=0')
        axes.set_ylim(0, 30)
        axes.set_xlim(0, 50)

        self.canvas = FigureCanvasTkAgg(figure, master=self.graph_frame)
        self.canvas.get_tk_widget().grid(row=0, column=0, sticky=N+S+E+W)

        self.slider = Scale(self.slider_frame, from_=0, to=10, length=400, width=30, orient=HORIZONTAL,
                            showvalue=0, bg="dark salmon")
        self.slider.grid(row=1, column=0, columnspan=2, sticky=E+W)
        self.slider_label = Label_class("Move slider to view graph", 0, 0, 12, 2, self.slider_frame)

        self.btn_play = Button(self.slider_frame, text="Play", bg="dark salmon", command=self.empty_buttons)
        self.btn_play.grid(row=2, column=0)
        self.btn_pause = Button(self.slider_frame, text="Pause", bg="dark salmon", command=self.empty_buttons)
        self.btn_pause.grid(row=2, column=1)

    ## Prompts an error message once the dummy buttons are clicked on.
    def empty_buttons(self):
        self.show_error("Please enter values for variables first")

    ## Prompts various error message when the user attempts to enter invalid values for certain variables.
    #
    #  @return False when an error is encountered, True otherwise.
    def check_values(self):
        if not self.selected.get():
            self.show_error("Please select forward/backward")
            return False
        if not self.methods.get():
            self.show_error("Please select Thomas/Inversion method")
            return False
        if not (self.x_min.get() and self.x_max.get() and self.dx.get() and self.t_min.get() and self.t_max.get() and self.dt.get() and self.f.get() and self.h.get() and self.g.get() and self.bc.get() and self.bc_bottom.get() and self.bc_top.get()):
            self.show_error("Please enter values for all variables")
            return False
        if not (self.is_number(self.x_min.get()) and self.is_number(self.x_max.get()) and self.is_number(self.dx.get()) and self.is_number(self.t_min.get()) and self.is_number(self.t_max.get()) and self.is_number(self.dt.get())):
            self.show_error("The following variables must be integers/floats: Minimum x, Maximum x, Minimum t, Maximum t, dx, dt")
            return False
        if float(self.dx.get()) == 0 or float(self.dt.get()) == 0:
            self.show_error("The value for dx and dt must not be 0")
            return False
        if float(self.x_max.get()) == float(self.x_min.get()) or float(self.t_max.get()) == float(self.t_min.get()):
            self.show_error("Minimum x must not equal Maximum x and Minimum t must not equal Maximum t")
            return False
        return True

    ## Prompts an error message to pop up.
    #
    #  @param message A string message to be displayed to the user.
    def show_error(self, message):
        tmb.showerror("Error", message)

    ## Determines whether x is a numerical including the type float.
    #
    #  @param x The possible numerical value.
    #  @return True if x is numerical, False otherwise.
    def is_number(self, x):
        try:
            float(x)
            return True
        except ValueError:
            return False

    ## Triggered by attempting to close the GUI, confirms with the user and then finishes the program.
    def close_app(self):
        if tmb.askokcancel("Close", "Exit window?"):
            sys.exit()


## Responsible for reading values from a specified file,
#  displaying a graph in a form of a matplotlib plot and updating the graph based on a specified time index.
class Grapher:
    ## The constructor
    #
    #  @param filename A CSV file containing the solution to a Kolmogorov equation.
    #  @param x A numpy array containing the values of the x variable.
    #  @param t A numpy array containing the values of the time variable.
    def __init__(self, filename, x, t):
        self.filename = filename
        self.x = x
        self.t = t
        try:
            self.y = np.zeros((len(t), len(x)))
        except:
            print('Error: Invalid values for x or t')
            sys.exit(1)
        self.plotLine = 0
        self.plotTitle = 0
        self.figure = 0

    ## Reads y values from a specified CSV file into a numpy array and transforms them to correspond
    #  to the following form: 1st row = minimum value of x, Nth row = maximum value of x,
    #  1st column = minimum value of t, Last column = maximum value of t.
    def read(self):
        try:
            f = open(self.filename, 'rb')
        except:
            print('Error: File could not be opened')
            sys.exit(1)
        lines = f.readlines()
        rows = np.genfromtxt(lines, delimiter=',', dtype='float')
        transpose = rows.transpose()
        self.y = np.flip(transpose,1)
        f.close()

    ## This function creates a blank matplot lib graph and a canvas on which to display the graph in the gui.
    # it is set to y = 0 initially.
    #
    # @param window is the Tkinter root interface in which the graph is to be displayed
    def display_graph(self,window):
        self.figure, axes = plt.subplots()
        axes.set_xlabel('x')
        axes.set_ylabel('p(x, t)')
        self.plotLine, = axes.plot(self.x, np.zeros(len(self.x)) * np.NaN, '-r')
        self.plotTitle = axes.set_title('t=0')
        axes.set_ylim(np.amin(self.y), np.amax(self.y))
        axes.set_xlim(np.amin(self.x), np.amax(self.x))
    
        canvas = FigureCanvasTkAgg(self.figure,master = window)
        canvas.get_tk_widget().grid(row=0, column=0, sticky=N+S+E+W)

    ## this function plots the Kolmogorov equation which corresponds to the time-index supplied to it from the slider
    #
    # @param time_index is the time index of the of the graph to be plotted
    def update_graph(self, time_index):
        ys = self.y[time_index]
        xs = self.x
        self.plotLine.set_ydata(ys)
        self.plotLine.set_xdata(xs)
        self.plotTitle.set_text(f"t = {self.t[time_index]:0.5f}")
        self.figure.canvas.draw()
            
def main():
    gui = tk_window()


if __name__ == "__main__":
    main()
