# A visualisation tool for the forward and backward Kolmogorov equations

This program is responsible for solving and visualising forward and backward Kolmogorov equations.

## Prerequisites 

The backend part of the program is programmed in C++, while the frontend is programmed in Python 3.
However, the program can be run on any Linux operating system without any prerequisites, as the two parts are combined into an executable using PyInstaller.

## Instructions

To run the program navigate through the Executables/Linux folder and run main.
A GUI will appear with several entry boxes/buttons on the left hand side and an empty graph on the right hand side.
Additionally, you can see the general Kolmogorov equation on the left hand side above the buttons.

To start, please enter all of the following variables:
```
1. Choose whether you would like to solve a forward or backward Kolmogorov equation by clicking on one of the radio buttons labelled as forward/backward.
2. Choose which method you would like to use while solving, by clicking on one of the radio buttons labelled as Thomas algorithm/Inversion method.
3. Choose the minimum value of x, maximum value of x and dx.
4. Choose the minimun value of t, maximum value of t and dt.
5. Choose the values for f, g and h.
6. Choose the values for the terminal conditon, bc left and bc right.
```
 
After you have entered all values, click on the "Calculate graph values" button to solve your specified Kolmogorov equation.
The result will be displayed on the right hand side, as a graph with x-axis indicating the x variable and y-axis indicating p(x, t).
To observe the effect of time, feel free to move the slider below the graph.
Additionally, you can animate the graph, starting from the current slider position, by clicking on the "Play" button and pause the animation by clicking on the "Pause" button.
After you are done analysing the solution, you can either solve another equation by following the process described above or exit the program by clicking on the x in the upper right corner.

### Syntax

This is the syntax for string input into the fields in the software.
See this link: https://github.com/ArashPartow/exprtk

## Authors
* **Hedi Chaibi** - *Backend* 
* **Lucia Simkanin** - *Frontend* 
* **Omolara Lebard** - *Frontend* 
* **Sami Khan** - *Backend* 

## Supervisor
* **Dr. Panos Parpas**